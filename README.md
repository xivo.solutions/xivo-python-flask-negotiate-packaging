# The packaging information for python python-flask-negotiate in XiVO

This repository contains the packaging information for
[python-flask-negotiate ](https://github.com/mattupstate/flask-negotiate).

To get a new version of python-flask-negotiate in the XiVO repository, set the
desired version in the `VERSION` file and increment the changelog.

[Jenkins](jenkins.xivo.io) will then retrieve and build the new version.

